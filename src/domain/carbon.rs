use crate::domain::energy::KiloWattHour;
use std::fmt::{Display, Formatter};
use std::ops::{Add, Mul};

/// A CO2 equivalent mass of greenhouse effect gas
#[derive(PartialEq, PartialOrd, Debug, Copy, Clone)]
pub struct FootPrint(f32);

impl Display for FootPrint {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{} gCO2eq", self.0)
    }
}

impl Add for FootPrint {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        FootPrint(self.0 + rhs.0)
    }
}

impl FootPrint {
    pub fn zero() -> FootPrint {
        FootPrint(0.)
    }
}

/// How carbon intense an energy consumption is
#[derive(Eq, PartialEq, Ord, PartialOrd, Debug, Copy, Clone)]
pub struct Intensity(u32);

impl Display for Intensity {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{} gCO2eq/kWh", self.0)
    }
}

impl Mul<KiloWattHour> for Intensity {
    type Output = FootPrint;

    fn mul(self, rhs: KiloWattHour) -> Self::Output {
        FootPrint(self.0 as f32 * rhs.0)
    }
}

impl Mul<Intensity> for KiloWattHour {
    type Output = FootPrint;

    fn mul(self, rhs: Intensity) -> Self::Output {
        rhs * self
    }
}

impl From<i32> for Intensity {
    fn from(g_co2_eq_per_kwh: i32) -> Self {
        Intensity(g_co2_eq_per_kwh as u32)
    }
}
