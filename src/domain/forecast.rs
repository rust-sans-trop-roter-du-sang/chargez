use std::fmt::{Display, Formatter};
use std::ops::Add;

use Error::*;

use crate::domain::carbon::{FootPrint, Intensity};
use crate::domain::energy::KiloWatt;
use crate::domain::time::Hour;

pub trait IntensityForecaster {
    /// Retrieve the next day carbon intensity forecast for a given zone.
    /// When Some, the returned Option should contain the forecast: an ordered Vec of size 24 (one
    /// intensity per hour).
    fn next_day_forecast(&self, zone: &str) -> Option<Vec<Intensity>>;
}

pub struct Consumption {
    pub start: Hour,
    pub speed: KiloWatt,
    pub duration: Hour,
    pub zone: String,
}

impl Consumption {
    fn ends_after_midnight(&self) -> bool {
        self.start + self.duration >= Hour(24)
    }
}

pub enum Error {
    ConsumptionEndsAfterMidnight,
    ForecastRetrievalFailure,
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", match &self {
            ConsumptionEndsAfterMidnight => "Consumption ends after midnight",
            ForecastRetrievalFailure => "Could not retrieve intensity forecast",
        })
    }
}

/// Forecast the carbon footprint of an energy consumption for tomorrow.
pub fn forecast_tomorrow_footprint(
    consumption: Consumption,
    forecaster: impl IntensityForecaster,
) -> Result<FootPrint, Error> {
    if consumption.ends_after_midnight() {
        Err(ConsumptionEndsAfterMidnight)
    } else {
        let hourly_forecast = forecaster
            .next_day_forecast(&consumption.zone)
            .ok_or(ForecastRetrievalFailure)?;
        Ok(estimate_intensity(consumption, hourly_forecast))
    }
}

fn estimate_intensity(consumption: Consumption, hourly_forecast: Vec<Intensity>) -> FootPrint {
    let hourly_consumption = consumption.speed * Hour(1);
    hourly_forecast
        .into_iter()
        .skip(consumption.start.0 as usize)
        .take(consumption.duration.0 as usize)
        .map(|intensity| intensity * hourly_consumption)
        .fold(FootPrint::zero(), FootPrint::add)
}
