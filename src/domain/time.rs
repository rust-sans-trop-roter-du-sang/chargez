use std::fmt::{Display, Formatter};
use std::num::ParseIntError;
use std::ops::Add;
use std::str::FromStr;

#[derive(Eq, PartialEq, Ord, PartialOrd, Debug, Copy, Clone)]
pub struct Hour(pub u32);

impl Display for Hour {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{} h", self.0)
    }
}

impl Add for Hour {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        Hour(self.0 + rhs.0)
    }
}

impl FromStr for Hour {
    type Err = ParseIntError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(Hour(s.parse()?))
    }
}
