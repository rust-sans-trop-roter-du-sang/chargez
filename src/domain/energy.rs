use std::fmt::{Display, Formatter};
use std::num::ParseFloatError;
use std::ops::Mul;
use std::str::FromStr;

use super::time::Hour;

/// A unit of power ~> instantaneous energy consumption speed
#[derive(PartialEq, PartialOrd, Debug, Copy, Clone)]
pub struct KiloWatt(f32);

impl Display for KiloWatt {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{} kW", self.0)
    }
}

impl FromStr for KiloWatt {
    type Err = ParseFloatError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(KiloWatt(s.parse()?))
    }
}

impl Mul<Hour> for KiloWatt {
    type Output = KiloWattHour;

    fn mul(self, rhs: Hour) -> Self::Output {
        KiloWattHour(self.0 * rhs.0 as f32)
    }
}

impl Mul<KiloWatt> for Hour {
    type Output = KiloWattHour;

    fn mul(self, rhs: KiloWatt) -> Self::Output {
        rhs * self
    }
}

/// A quantity of energy, congruous to Joules
#[derive(PartialEq, PartialOrd, Debug, Copy, Clone)]
pub struct KiloWattHour(pub f32);

impl Display for KiloWattHour {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{} kWh", self.0)
    }
}
