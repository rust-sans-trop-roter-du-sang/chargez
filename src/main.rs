use std::process::exit;

use clap::Parser;

use crate::config::new_intensity_forecaster;
use crate::domain::energy::KiloWatt;
use crate::domain::forecast::{forecast_tomorrow_footprint, Consumption};
use crate::domain::time::Hour;

mod domain;
mod reqwest;
mod stub;

fn main() {
    let args = CliArguments::parse();

    println!(
        "Forecasting footprint for an electricity consumption rate of {} for {} starting tomorrow at {} in zone {}…",
        args.speed,
        args.duration,
        args.starting_at,
        args.zone
    );

    match forecast_tomorrow_footprint(args.to_consumption(), new_intensity_forecaster()) {
        Ok(footprint) => {
            println!("Forecasted footprint: {}", footprint);
            exit(0);
        }
        Err(error) => {
            eprintln!("Failed: {}", error);
            exit(2);
        }
    }
}

#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
struct CliArguments {
    /// Consumption speed, in kW
    #[arg(short = 'k', long)]
    speed: KiloWatt,

    /// Consumption duration, in hours
    #[arg(short, long)]
    duration: Hour,

    /// Consumption start hour
    #[arg(short, long)]
    starting_at: Hour,

    /// Zone where the consumption would happen
    #[arg(short, long, default_value_t = String::from("FR"))]
    zone: String,
}

impl CliArguments {
    fn to_consumption(self) -> Consumption {
        Consumption {
            speed: self.speed,
            duration: self.duration,
            start: self.starting_at,
            zone: self.zone,
        }
    }
}

#[allow(unused_imports, dead_code)]
mod config {
    use std::env;

    use crate::domain::forecast::IntensityForecaster;
    use crate::reqwest::electricity_maps::RestClient;

    pub fn new_intensity_forecaster() -> impl IntensityForecaster {
        RestClient::new(
            require_env_var("ELECTRICITY_MAPS_CLIENT_ID"),
            require_env_var("ELECTRICITY_MAPS_ACCESS_TOKEN"),
        )
    }

    fn require_env_var(key: &str) -> String {
        env::var(key).expect(&format!("Required environment variable is missing: {}", key))
    }
}
