use std::str::FromStr;

use reqwest::blocking::Client;
use reqwest::header::{HeaderMap, HeaderName, HeaderValue};

pub mod electricity_maps {
    use std::cmp::Ordering;

    use iso8601_timestamp::Timestamp;
    use reqwest::blocking::{Client, Response};
    use reqwest::Error;
    use serde::Deserialize;
    use time::Date;

    use crate::domain::carbon::Intensity;
    use crate::domain::forecast::IntensityForecaster;

    pub struct RestClient {
        inner: Client,
        base_url: String,
    }

    impl RestClient {
        pub fn new(client_id: String, access_token: String) -> impl IntensityForecaster {
            RestClient {
                inner: super::authenticated_client("X-Blobr-Key", access_token),
                base_url: format!("https://api-access.electricitymaps.com/{}", client_id),
            }
        }

        fn get_carbon_intensity_forecast(&self, zone: &str) -> Result<ForecastResponse, Error> {
            self.inner
                .get(format!("{}/carbon-intensity/forecast", self.base_url))
                .query(&[("zone", zone)])
                .send()
                .and_then(Response::json::<ForecastResponse>)
        }
    }

    impl IntensityForecaster for RestClient {
        fn next_day_forecast(&self, zone: &str) -> Option<Vec<Intensity>> {
            match self.get_carbon_intensity_forecast(zone) {
                Ok(response) => response.to_hourly_forecast(tomorrow()),
                Err(_) => None,
            }
        }
    }

    fn tomorrow() -> Date {
        Timestamp::now_utc().date().next_day().unwrap()
    }

    #[derive(Deserialize)]
    struct ForecastResponse {
        forecast: Vec<Forecast>,
    }

    #[derive(Deserialize)]
    struct Forecast {
        #[serde(alias = "carbonIntensity")]
        carbon_intensity: i32,
        datetime: Timestamp,
    }

    impl ForecastResponse {
        fn to_hourly_forecast(mut self, date: Date) -> Option<Vec<Intensity>> {
            self.forecast.retain(|forecast| forecast.is_for(&date));
            if self.forecast.len() == 24 {
                self.forecast.sort_by(|one, other| one.datetime_cmp(other));
                Some(self.to_intensities())
            } else {
                None
            }
        }

        fn to_intensities(&self) -> Vec<Intensity> {
            self.forecast
                .iter()
                .map(|forecast| Intensity::from(forecast.carbon_intensity))
                .collect()
        }
    }

    impl Forecast {
        fn is_for(&self, date: &Date) -> bool {
            &self.datetime.date() == date
        }

        fn datetime_cmp(&self, rhs: &Forecast) -> Ordering {
            self.datetime.cmp(&rhs.datetime)
        }
    }
}

fn authenticated_client(header_name: &str, header_value: String) -> Client {
    Client::builder()
        .default_headers(authentication_header(header_name, header_value))
        .build()
        .expect("Could not initialize TLS backend")
}

fn authentication_header(name: &str, value: String) -> HeaderMap {
    let mut map = HeaderMap::new();
    map.append(
        HeaderName::from_str(name).expect("Invalid header name"),
        sensitive_header_value(value),
    );
    map
}

fn sensitive_header_value(value: String) -> HeaderValue {
    let mut value = HeaderValue::from_str(&value).expect("Invalid header value");
    value.set_sensitive(true);
    value
}
