pub mod electricity_maps {
    use crate::domain::carbon::Intensity;
    use crate::domain::forecast::IntensityForecaster;

    pub struct StubClient;

    impl IntensityForecaster for StubClient {
        fn next_day_forecast(&self, _: &str) -> Option<Vec<Intensity>> {
            Some(vec![
                85, 85, 84, 84, 85, 87, 92, 94, 96, 97, 94, 78,
                76, 75, 73, 74, 76, 85, 91, 93, 95, 95, 92, 85,
            ].into_iter().map(Intensity::from).collect())
        }
    }
}
